#include "find_siblings.h"

char *checkingForBrothers;
struct pais *checkingForBrothersParents;
static GHashTable *info;
static GHashTable *filhosPais;

static void checkSib(gpointer key, gpointer value, gpointer userdata) {

    GSList *siblings = NULL;

    struct pais *currParents = value;

    if ((currParents->mae != NULL) && (currParents->pai != NULL) &&
        (checkingForBrothersParents->mae != NULL) &&
        (checkingForBrothersParents->pai != NULL)) {

        if ((strcmp(currParents->mae, checkingForBrothersParents->mae) == 0) &&
            (strcmp(currParents->pai, checkingForBrothersParents->pai) == 0)) {

            if (strcmp(checkingForBrothers, key) != 0) {

                siblings =
                    g_slist_append(siblings, strdup(checkingForBrothers));

                char *sibling = key;

                struct personalInfo *persInfo =
                    g_hash_table_lookup(info, checkingForBrothers);
                struct personalInfo *persInfo2 =
                    g_hash_table_lookup(info, sibling);

                struct relationship *relation =
                    malloc(sizeof(struct relationship));
                relation->targetURI = strdup(sibling);
                if (persInfo2->gender == Female) {
                    relation->relation = temIrma;
                } else {
                    relation->relation = temIrmao;
                }
                persInfo->relationships =
                    g_slist_append(persInfo->relationships, relation);
                g_hash_table_replace(info, checkingForBrothers, persInfo);
            }
        }
    }
}

static void searchForSiblings(gpointer key, gpointer value, gpointer userdata) {

    checkingForBrothers = strdup(key);

    checkingForBrothersParents = value;

    g_hash_table_foreach(filhosPais, checkSib, NULL);
}

void findSiblings(GHashTable *mainInfo, GHashTable *mainFilhosPais) {
    info = mainInfo;
    filhosPais = mainFilhosPais;
    g_hash_table_foreach(filhosPais, searchForSiblings, NULL);
}
