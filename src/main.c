#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <argp.h>
#include <error.h>

#include <fcntl.h>
#include <unistd.h>

#include <glib.h>
#include <gmodule.h>

extern int yyparse();
extern void init(GHashTable *, GHashTable *);
extern void dumpInfoHashTableToDot();
extern void dumpParentsHashTableToDot();

#include "find_cousins.h"
#include "find_grandparents.h"
#include "find_siblings.h"
#include "find_uncles.h"

GHashTable *info;
GHashTable *filhosPais;

const char *argp_program_version = "Visualizador de Árvores Genealógicas-alpha";

/* Program documentation. */
static char doc[] = "familytree - Visualizador de Árvores Genealógicas";

/* A description of the arguments we accept. */
static char args_doc[] = "INPUT";

/* The options we understand. */
static struct argp_option options[] = {
    {"debug", 'd', 0, 0, "Print debug information", 0},
    {"output", 'o', "IMAGE", 0, "Name of image", 0},
    {0}};

/* Used by main to communicate with parse_opt. */
struct arguments {
    char *input_file;
    char *output_file;
    int debug;
};

/* Parse a single option. */
static error_t parse_opt(int key, char *arg, struct argp_state *state) {
    struct arguments *arguments = state->input;

    switch (key) {
        case 'd':
            arguments->debug = 1;
            break;
        case 'o':
            arguments->output_file = arg;
            break;

        case ARGP_KEY_ARG:
            if (state->arg_num >= 1) {
                /* Too many arguments. */
                argp_usage(state);
            }

            arguments->input_file = arg;
            break;

        case ARGP_KEY_END:
            if (state->arg_num < 1)
                /* Not enough arguments. */
                argp_usage(state);
            break;

        default:
            return ARGP_ERR_UNKNOWN;
    }

    return 0;
}

/* Our argp parser. */
static struct argp argp = {options, parse_opt, args_doc, doc, NULL, NULL, NULL};

int main(int argc, char **argv) {

    struct arguments arguments;

    char *dotfile = "/tmp/graph.dot";

    /* Default values. */
    arguments.debug = 0;
    arguments.output_file = "tree.svg";

    argp_parse(&argp, argc, argv, 0, 0, &arguments);

    if (arguments.debug) {
        printf("INPUT FILE = %s\n", arguments.input_file);

        printf("OUTPUT_FILE = %s\nDEBUG = %s\n", arguments.output_file,
               arguments.debug ? "yes" : "no");
    }

    printf("\n");

    info = g_hash_table_new(g_str_hash, g_str_equal);
    filhosPais = g_hash_table_new(g_str_hash, g_str_equal);

    init(info, filhosPais);

    fopen(dotfile, "w+");

    dup2(open(arguments.input_file, O_RDONLY), 0);

    dup2(open(dotfile, O_WRONLY), 1);

    printf("digraph {\n");

    yyparse();

    findSiblings(info, filhosPais);
    findGrandparents(info, filhosPais);
    findUncles(info, filhosPais);
    findCousins(info, filhosPais);

    dumpInfoHashTableToDot();
    dumpParentsHashTableToDot();

    printf("}\n");

    char *command;

    asprintf(&command, "dot -Tsvg %s -o %s", dotfile, arguments.output_file);

    system(command);

    return 0;
}
