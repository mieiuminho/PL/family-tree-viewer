%{

#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>
#include <gmodule.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include "global.h"
#include "structs.h"
#include "utils.h"

extern int yylex();

extern int yylineno;

extern char* yytext;

int yyerror();

int erroSem(char*);

extern int lerInt();

#define URI 0
#define INT 1
#define FLOAT 2
#define STR 3

static GHashTable *info;
static GHashTable *filhosPais;

void init(GHashTable *mainInfo, GHashTable *mainFilhosPais) {
    info = mainInfo;
    filhosPais = mainFilhosPais;
}

void execute(char *subject, char *predicate, struct elem *s, GSList *list);

struct elem* parseUri(char *uri) {
    struct elem *s = malloc(sizeof(struct elem));
    union objectValue r;
    r.strVal = strdup(uri);
    s->valor = r;
    s->type = URI;
    return s;
}

struct elem* parseInt(int integer) {
    struct elem *s = malloc(sizeof(struct elem));
    union objectValue r;
    r.intVal = integer;
    s->valor = r;
    s->type = INT;
    return s;
}

struct elem* parseFloat(float val) {
    struct elem *s = malloc(sizeof(struct elem));
    union objectValue r;
    r.fltVal = val;
    s->valor = r;
    s->type = FLOAT;
    return s;
}

struct elem* parseString(char *string) {
    struct elem *s = malloc(sizeof(struct elem));
    union objectValue r;
    r.strVal = strdup(string);
    s->valor = r;
    s->type = STR;
    return s;
}

GSList* recognizeCommaObject(struct elem *object) {
    GSList *list = NULL;
    GSList *innerList = NULL;
    innerList = g_slist_append(innerList, object);
    list = g_slist_append(list, innerList);
    return list;
}

GSList* recognizeSemicolonUriObject(char *uri, struct elem *object) {
    GSList *list = NULL;
    GSList *innerList = NULL;
    union objectValue r;
    struct elem *uriObject = malloc(sizeof(struct elem));
    r.strVal = strdup(uri);
    uriObject->valor = r;
    uriObject->type = URI;
    innerList = g_slist_append(innerList, uriObject);
    innerList = g_slist_append(innerList, object);
    list = g_slist_append(list, innerList);
    return list;
}

GSList* recognizeSemicolonUriUri(char *uri1, struct elem *object) {
    struct elem *uri1object = malloc(sizeof(struct elem));
    union objectValue r;
    r.strVal = strdup(uri1);
    uri1object->valor = r;
    uri1object->type = URI;
    GSList *innerList = NULL;
    innerList = g_slist_append(innerList, uri1object);
    innerList = g_slist_append(innerList, object);
    return innerList;
}

GSList *recognizeCommaUri(struct elem *object) {
    GSList *innerList = NULL;
    innerList = g_slist_append(innerList, object);
    return innerList;
}

void execute(char *subject, char *predicate, struct elem *s, GSList *list) {
    char *currentPredicate = strdup(predicate);
    switch(s->type) {
        case URI:
        case STR:
            printf("\t\"%s\" -> \"%s\" [label=\"%s\"];\n", subject, s->valor.strVal, predicate);
            break;
        case FLOAT:
            printf("\t\"%s\" -> \"%f\" [label=\"%s\"];\n", subject, s->valor.fltVal, predicate);
            break;
        case INT:
            printf("\t\"%s\" -> \"%d\" [label=\"%s\"];\n", subject, s->valor.intVal, predicate);
            break;
    }
    if(list != NULL) {
        guint size = g_slist_length(list);
        for(guint i = 0; i < size; i++) {
            GSList *innerList = g_slist_nth_data(list, i);
            guint innerSize = g_slist_length(innerList);
            if(innerSize == 1) {
                struct elem *s = g_slist_nth_data(innerList, 0);
                switch(s->type) {
                    case URI:
                        if(strcmp(currentPredicate, "rdf:type") == 0) {
                            if(strcmp(s->valor.strVal, ":Pessoa") == 0) {
                                printf("\t\"%s\" -> \"%s\" [label=\"%s\"];\n", subject, s->valor.strVal, currentPredicate);
                                struct personalInfo *infoPer = malloc(sizeof(struct personalInfo));
                                GSList *list = NULL;
                                infoPer->personURI = strdup(subject);
                                infoPer->gender = Unknown;
                                infoPer->relationships = list;
                                g_hash_table_insert(info, strdup(subject), infoPer);
                                struct pais *parents = malloc(sizeof(struct pais));
                                parents->pai = NULL;
                                parents->mae = NULL;
                                g_hash_table_insert(filhosPais, strdup(subject), parents);
                            } else {
                                if(strcmp(s->valor.strVal, ":Masculino") == 0) {
                                    struct personalInfo *p = g_hash_table_lookup(info, subject);
                                    p->gender = Male;
                                    g_hash_table_replace(info, strdup(subject), p);
                                } else if(strcmp(s->valor.strVal, ":Feminino") == 0) {
                                    struct personalInfo *n = g_hash_table_lookup(info, subject);
                                    n->gender = Female;
                                    g_hash_table_replace(info, subject, n);
                                }

                            }
                        } else if(strcmp(currentPredicate, ":temPai") == 0) {
                                    struct pais *pars = g_hash_table_lookup(filhosPais, subject);
                                    pars->pai = strdup(s->valor.strVal);
                                    g_hash_table_replace(filhosPais, strdup(subject), pars);
                        } else if(strcmp(currentPredicate, ":temMae") == 0) {
                                    struct pais *pars = g_hash_table_lookup(filhosPais, subject);
                                    pars->mae = strdup(s->valor.strVal);
                                    g_hash_table_replace(filhosPais, strdup(subject), pars);
                        }
                        break;
                    case INT:
                        printf("\t\"%s\" -> %d [label=\"%s\"];\n", subject, s->valor.intVal, currentPredicate);
                        break;
                    case FLOAT:
                        printf("\t\"%s\" -> %f [label=\"%s\"];\n", subject, s->valor.fltVal, currentPredicate);
                        break;
                    case STR:
                        printf("AQUI\t\"%s\" -> \"%s\" [label=\"%s\"];\n", subject, s->valor.strVal, currentPredicate);
                        break;
                }
            }
            if(innerSize == 2) {
                struct elem *s0 = g_slist_nth_data(innerList, 0);
                struct elem *s1 = g_slist_nth_data(innerList, 1);
                currentPredicate = strdup(s0->valor.strVal);
                switch(s1->type) {
                    case URI:
                        if(strcmp(currentPredicate, "rdf:type") == 0) {
                            if(strcmp(s1->valor.strVal, ":Pessoa") == 0) {
                                struct personalInfo *infoPer = malloc(sizeof(struct personalInfo));
                                GSList *list = NULL;
                                infoPer->personURI = strdup(subject);
                                infoPer->gender = Unknown;
                                infoPer->relationships = list;
                                g_hash_table_insert(info, strdup(subject), infoPer);
                                struct pais *parents = malloc(sizeof(struct pais));
                                parents->pai = NULL;
                                parents->mae = NULL;
                                g_hash_table_insert(filhosPais, strdup(subject), parents);
                            } else {

                                if(strcmp(s1->valor.strVal, ":Masculino") == 0) {
                                    struct personalInfo *p = g_hash_table_lookup(info, subject);
                                    p->gender = Male;
                                    g_hash_table_replace(info, subject, p);
                                } else if(strcmp(s1->valor.strVal, ":Feminino") == 0) {
                                    struct personalInfo *n = g_hash_table_lookup(info, subject);
                                    n->gender = Female;
                                    g_hash_table_replace(info, strdup(subject), n);
                                }

                            }

                        } else if(strcmp(currentPredicate, ":temPai") == 0) {
                                    struct pais *pars = g_hash_table_lookup(filhosPais, subject);
                                    pars->pai = strdup(s1->valor.strVal);
                                    g_hash_table_replace(filhosPais, strdup(subject), pars);
                        } else if(strcmp(currentPredicate, ":temMae") == 0) {
                                    struct pais *pars = g_hash_table_lookup(filhosPais, subject);
                                    pars->mae = strdup(s1->valor.strVal);
                                    g_hash_table_replace(filhosPais, strdup(subject), pars);
                        }
                        break;
                    case INT:
                        printf("\t\"%s\" -> %d [label=\"%s\"];\n", subject, s1->valor.intVal, currentPredicate);
                        break;
                    case FLOAT:
                        printf("\t\"%s\" -> %f [label=\"%s\"];\n", subject, s1->valor.fltVal, currentPredicate);
                        break;
                    case STR:
                        printf("AQUI\t\"%s\" -> \"%s\" [label=\"%s\"];\n", subject, s1->valor.strVal, currentPredicate);
                        break;
                }
            }
        }
    }
}

%}

%union {
    int intVal;
    float fltVal;
    char* strVal;
    GSList *lista;
    struct elem *objeto;
};

%token POINT COMMA SEMICOLON
%token <intVal> inteiro
%token <fltVal> decimal
%token <strVal> uri
%token <strVal> str
%type <lista> RECURSIVE
%type <objeto> OBJECT


%%

FILE: TRIPLE FILE
    | TRIPLE
    ;

TRIPLE: uri uri OBJECT RECURSIVE POINT {
            execute($1, $2, $3, $4);
      }
      | uri uri OBJECT POINT {
            execute($1,$2, $3, NULL);
      }
      ;

RECURSIVE: COMMA OBJECT RECURSIVE {
             GSList *innerList = recognizeCommaUri($2);
             $$ = g_slist_prepend($3, innerList);
         }
         | SEMICOLON uri OBJECT RECURSIVE {
             GSList *innerList = recognizeSemicolonUriUri($2, $3);
             $$ = g_slist_prepend($4, innerList);
         }
         | COMMA OBJECT {
             $$ = recognizeCommaObject($2); }
         | SEMICOLON uri OBJECT {
             $$ = recognizeSemicolonUriObject($2, $3); }
         ;

OBJECT: uri {
            $$ = parseUri($1);
        }
      | inteiro {
            $$ = parseInt($1); 
        }
      | decimal {
            $$ = parseFloat($1);
        }
      | str {
            $$ = parseString($1);
        }
      ;

%%


static void iterateRelationships(gpointer element, gpointer userdata) {

    struct relationship *rel = element;
    char *subject = userdata;

    switch(rel->relation) {
            case temAvoF:
                printf("\t\"%s\" -> \"%s\" [label=\"%s\"];\n", subject, rel->targetURI, ":temAvoF");
                break;
            case temAvoM:
                printf("\t\"%s\" -> \"%s\" [label=\"%s\"];\n", subject, rel->targetURI, ":temAvoM");
                break;
            case temIrma:
                printf("\t\"%s\" -> \"%s\" [label=\"%s\"];\n", subject, rel->targetURI, ":temIrma");
                break;
            case temIrmao:
                printf("\t\"%s\" -> \"%s\" [label=\"%s\"];\n", subject, rel->targetURI, ":temIrmao");
                break;
            case temMae:
                printf("\t\"%s\" -> \"%s\" [label=\"%s\"];\n", subject, rel->targetURI, ":temMae");
                break;
            case temPai:
                printf("\t\"%s\" -> \"%s\" [label=\"%s\"];\n", subject, rel->targetURI, ":temPai");
                break;
            case temTia:
                printf("\t\"%s\" -> \"%s\" [label=\"%s\"];\n", subject, rel->targetURI, ":temTia");
                break;
            case temTio:
                printf("\t\"%s\" -> \"%s\" [label=\"%s\"];\n", subject, rel->targetURI, ":temTio");
                break;
            case temPrimo:
                printf("\t\"%s\" -> \"%s\" [label=\"%s\"];\n", subject, rel->targetURI, ":temPrimo");
                break;
            case temPrima:
                printf("\t\"%s\" -> \"%s\" [label=\"%s\"];\n", subject, rel->targetURI, ":temPrima");
                break;
        }

}

static void dumpEntry(gpointer key, gpointer value, gpointer userdata) {

    char *subject = key;

    struct personalInfo *persInfo = value;

    GSList *relationships = persInfo->relationships;

    enum Gender m = persInfo->gender;

    if(m == Male) {
        printf("\t\"%s\" -> \"%s\" [label=\"%s\"];\n", subject, ":Male", ":temGenero");
    } else if(m == Female) {
        printf("\t\"%s\" -> \"%s\" [label=\"%s\"];\n", subject, ":Female", ":temGenero");
    }

    g_slist_foreach(relationships, iterateRelationships, subject);

}

void dumpInfoHashTableToDot() {

    g_hash_table_foreach(info, dumpEntry, NULL);

}

static void dumpEntryParents(gpointer key, gpointer value, gpointer userdata) {

    char *subject = key;

    struct pais *parents = value;

    if(parents->mae != NULL) {
        printf("\t\"%s\" -> \"%s\" [label=\"%s\"];\n", subject, parents->mae, ":temMae");
    }

    if(parents->pai != NULL) {
        printf("\t\"%s\" -> \"%s\" [label=\"%s\"];\n", subject, parents->pai, ":temPai");
    }

}

void dumpParentsHashTableToDot() {

    g_hash_table_foreach(filhosPais, dumpEntryParents, NULL);
}

int erroSem(char *s) {
    char *error_message;
    asprintf(&error_message, "Erro Semantico na linha: %d, %s...\n", yylineno, s); 
    print_error(error_message);
    return 0;
}

int yyerror() {
    char *error_message;
    asprintf(&error_message, "Erro Sintático ou Léxico na linha: %d, com o texto: %s\n", yylineno, yytext);
    print_error(error_message);
    return 0;
}