#include "find_grandparents.h"

static GHashTable *info;
static GHashTable *filhosPais;

static void searchForGrandparents(gpointer key, gpointer value,
                                  gpointer userdata) {

    char *currentGrandSon = strdup(key);

    struct pais *grandsonsParents = value;

    char *mae = NULL;
    char *pai = NULL;

    if (grandsonsParents->mae != NULL) {
        mae = strdup(grandsonsParents->mae);
    }

    if (grandsonsParents->pai != NULL) {
        pai = strdup(grandsonsParents->pai);
    }

    struct pais *grandsonsFatherParents = NULL;
    struct pais *grandsonsMotherParents = NULL;

    struct personalInfo *persInfo = g_hash_table_lookup(info, currentGrandSon);

    if (mae != NULL) {

        grandsonsMotherParents = g_hash_table_lookup(filhosPais, mae);

        if (grandsonsMotherParents != NULL) {

            char *avoMaterna = NULL;
            char *avoMaterno = NULL;

            if (grandsonsMotherParents->mae != NULL) {
                avoMaterna = strdup(grandsonsMotherParents->mae);
            }

            if (grandsonsMotherParents->pai != NULL) {
                avoMaterno = strdup(grandsonsMotherParents->pai);
            }

            if (avoMaterna != NULL) {
                struct relationship *rel1 = malloc(sizeof(struct relationship));
                rel1->targetURI = strdup(avoMaterna);
                rel1->relation = temAvoF;
                persInfo->relationships =
                    g_slist_append(persInfo->relationships, rel1);
            }

            if (avoMaterno != NULL) {
                struct relationship *rel2 = malloc(sizeof(struct relationship));
                rel2->targetURI = strdup(avoMaterno);
                rel2->relation = temAvoM;
                persInfo->relationships =
                    g_slist_append(persInfo->relationships, rel2);
            }
        }
    }

    if (pai != NULL) {

        grandsonsFatherParents = g_hash_table_lookup(filhosPais, pai);

        if (grandsonsFatherParents != NULL) {

            char *avoPaterna = NULL;
            char *avoPaterno = NULL;

            if (grandsonsFatherParents->mae != NULL) {
                avoPaterna = strdup(grandsonsFatherParents->mae);
            }

            if (grandsonsFatherParents->pai != NULL) {
                avoPaterno = strdup(grandsonsFatherParents->pai);
            }

            if (avoPaterna != NULL) {
                struct relationship *rel3 = malloc(sizeof(struct relationship));
                rel3->targetURI = strdup(avoPaterna);
                rel3->relation = temAvoF;
                persInfo->relationships =
                    g_slist_append(persInfo->relationships, rel3);
            }

            if (avoPaterno != NULL) {
                struct relationship *rel4 = malloc(sizeof(struct relationship));
                rel4->targetURI = strdup(avoPaterno);
                rel4->relation = temAvoM;
                persInfo->relationships =
                    g_slist_append(persInfo->relationships, rel4);
            }
        }
    }

    g_hash_table_replace(info, currentGrandSon, persInfo);
}

void findGrandparents(GHashTable *mainInfo, GHashTable *mainFilhosPais) {
    info = mainInfo;
    filhosPais = mainFilhosPais;
    g_hash_table_foreach(filhosPais, searchForGrandparents, NULL);
    ;
}
