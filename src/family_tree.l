%{
#include <gmodule.h>
#include "yacc_family_tree.h"
%}

%%

[ \t\n\r]             ;
#(.)*\n               {;}
[+-]?[0-9]+\.[0-9]+   {yylval.fltVal = atof(yytext); return decimal;}
[+-]?[0-9]+           {yylval.intVal = atoi(yytext); return inteiro;}
[^ \.;,\n]*:[^ \.;,\n]+   {yylval.strVal = strdup(yytext); return uri;}
\"[^"]*\"             {yylval.strVal = strdup(yytext); return str;}
\.                    {return POINT;}
,                     {return COMMA;}
;                     {return SEMICOLON;}
.                     {;}

%%
