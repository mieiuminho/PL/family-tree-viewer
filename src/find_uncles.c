#include "find_uncles.h"

static GHashTable *info;
static GHashTable *filhosPais;
GSList *currentUncles = NULL;

static void addBrothersToUnclesList(gpointer element, gpointer userdata) {

    struct relationship *rel = element;

    if ((rel->relation == temIrmao) || (rel->relation == temIrma)) {
        currentUncles = g_slist_append(currentUncles, strdup(rel->targetURI));
    }
}

static void addUncleToNephew(gpointer element, gpointer userdata) {

    char *currentNephew = strdup(userdata);

    struct personalInfo *nephewInfo = g_hash_table_lookup(info, currentNephew);
    struct personalInfo *uncleInfo = g_hash_table_lookup(info, element);

    struct relationship *rel = malloc(sizeof(struct relationship));

    rel->targetURI = strdup(element);
    if (uncleInfo->gender == Female) {
        rel->relation = temTia;
    } else {
        rel->relation = temTio;
    }

    nephewInfo->relationships = g_slist_append(nephewInfo->relationships, rel);
    g_hash_table_replace(info, currentNephew, nephewInfo);
}

static void searchForUncles(gpointer key, gpointer value, gpointer userdata) {

    char *currentNephew = strdup(key);

    struct pais *parents = value;

    struct personalInfo *motherInfo = NULL;
    struct personalInfo *fatherInfo = NULL;

    if (parents->mae != NULL) {
        motherInfo = g_hash_table_lookup(info, parents->mae);
    }

    if (parents->pai != NULL) {
        fatherInfo = g_hash_table_lookup(info, parents->pai);
    }

    if (motherInfo != NULL) {
        GSList *motherRelationships = motherInfo->relationships;
        g_slist_foreach(motherRelationships, addBrothersToUnclesList, NULL);
    }

    if (fatherInfo != NULL) {
        GSList *fatherRelationships = fatherInfo->relationships;
        g_slist_foreach(fatherRelationships, addBrothersToUnclesList, NULL);
    }

    g_slist_foreach(currentUncles, addUncleToNephew, currentNephew);

    currentUncles = NULL;
}

void findUncles(GHashTable *mainInfo, GHashTable *mainFilhosPais) {
    info = mainInfo;
    filhosPais = mainFilhosPais;
    g_hash_table_foreach(filhosPais, searchForUncles, NULL);
}
