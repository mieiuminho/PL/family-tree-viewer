#include "find_cousins.h"
#include "global.h"
#include "structs.h"

int r = 0;
char *checkingForCousins;
char *toCompare;
static GHashTable *info;
static GHashTable *filhosPais;

static void searchForBrotherRelation(gpointer element, gpointer userdata) {

    relationship rel = element;

    if ((r == 0) && (rel != NULL)) {

        if (strcmp(rel->targetURI, toCompare) == 0) {
            if ((rel->relation == temIrmao) || (rel->relation == temIrma)) {
                r = 1;
            }
        }
    }
}

int areBrothers(char *s1, char *s2) {

    if ((s1 != NULL) && (s2 != NULL)) {

        toCompare = strdup(s2);
        r = 0;

        personalInfo persInfo = g_hash_table_lookup(info, s1);

        if (persInfo != NULL) {

            g_slist_foreach(persInfo->relationships, searchForBrotherRelation,
                            NULL);
        }

        return r;

    } else
        return 0;
}

int areTheyCousins(pais p1, pais p2) {

    char *mae1 = NULL;

    if (p1->mae != NULL) {
        mae1 = strdup(p1->mae);
    }

    char *pai1 = NULL;

    if (p1->pai != NULL) {
        pai1 = strdup(p1->pai);
    }

    char *mae2 = NULL;

    if (p2->mae != NULL) {
        mae2 = strdup(p2->mae);
    }

    char *pai2 = NULL;

    if (p2->pai != NULL) {
        pai2 = strdup(p2->pai);
    }

    return (areBrothers(mae1, mae2) || areBrothers(mae1, pai2) ||
            areBrothers(pai1, mae2) || areBrothers(pai1, pai2));
}

static void addCousins(gpointer key, gpointer value, gpointer userdata) {

    char *possibleCousin = strdup(key);

    pais possibleCousinsParents = value;
    pais subjectsParents = userdata;

    int r = areTheyCousins(subjectsParents, possibleCousinsParents);

    if (r) {

        personalInfo subject = g_hash_table_lookup(info, checkingForCousins);
        personalInfo cousin = g_hash_table_lookup(info, possibleCousin);

        relationship rel = malloc(sizeof(struct relationship));

        rel->targetURI = strdup(possibleCousin);

        if (cousin->gender == Female) {
            rel->relation = temPrima;
        } else {
            rel->relation = temPrimo;
        }

        subject->relationships = g_slist_append(subject->relationships, rel);
        g_hash_table_replace(info, checkingForCousins, subject);
    }
}

static void searchForCousins(gpointer key, gpointer value, gpointer userdata) {

    checkingForCousins = strdup(key);

    pais parents = value;

    g_hash_table_foreach(filhosPais, addCousins, parents);
}

void findCousins(GHashTable *mainInfo, GHashTable *mainFilhosPais) {
    info = mainInfo;
    filhosPais = mainFilhosPais;
    g_hash_table_foreach(filhosPais, searchForCousins, NULL);
}
