#==============================================================================
SHELL   = bash
#------------------------------------------------------------------------------
CC      = gcc
LD      = gcc
CFLAGS  = -O2 -Wall -Wextra
CFLAGS += -Wno-unused-parameter -Wno-unused-function -Wno-unused-result
LIBS    = `pkg-config --cflags --libs glib-2.0` -lfl
INCLDS  = -I $(INC_DIR)
#------------------------------------------------------------------------------
BIN_DIR = bin
BLD_DIR = build
DOC_DIR = docs
INC_DIR = includes
LOG_DIR = log
OUT_DIR = out
SRC_DIR = src
TST_DIR = tests
UTI_DIR = scripts
#------------------------------------------------------------------------------
TRASH   = $(BIN_DIR) $(BLD_DIR) $(DOC_DIR)/{html,latex} $(LOG_DIR) $(OUT_DIR)
#------------------------------------------------------------------------------
SRC     = $(wildcard $(SRC_DIR)/*.c)
LEX     = $(wildcard $(SRC_DIR)/*.l)
YACC    = $(wildcard $(SRC_DIR)/*.y)
CLEX    = $(patsubst $(SRC_DIR)/%.l,$(OUT_DIR)/lex_%.c,$(LEX))
CYACC   = $(patsubst $(SRC_DIR)/%.y,$(OUT_DIR)/yacc_%.c,$(YACC))
OBJS    = $(patsubst $(SRC_DIR)/%.c,$(BLD_DIR)/%.o,$(SRC))
OBJS   += $(patsubst $(OUT_DIR)/lex_%.c,$(BLD_DIR)/lex_%.o,$(wildcard $(OUT_DIR)/*.c))
OBJS   += $(patsubst $(OUT_DIR)/yacc_%.c,$(BLD_DIR)/yacc_%.o,$(wildcard $(OUT_DIR)/*.c))
DEPS    = $(patsubst $(BLD_DIR)/%.o,$(BLD_DIR)/%.d,$(OBJS))
PROGRAM = familytree
#==============================================================================

vpath %.c $(SRC_DIR)

.DEFAULT_GOAL = build

.PHONY: build check setup clean debug doc fmt lint run test

define show
	@./$(UTI_DIR)/change_output_color.sh $(1) $(2)
endef

init: setup yacc flex

yacc: $(CYACC)

flex: $(CLEX)

$(OUT_DIR)/lex_%.c: $(SRC_DIR)/%.l
	$(call show,magenta)
	flex -o $@ $<
	$(call show,,reset)

$(OUT_DIR)/yacc_%.c: $(SRC_DIR)/%.y
	$(call show,magenta)
	yacc --defines=$(basename $@).h -o $@ $<
	$(call show,,reset)

$(BLD_DIR)/lex_%.o: $(OUT_DIR)/lex_%.c
	$(call show,blue)
	$(CC) -c $(INCLDS) $(LIBS) $(CFLAGS) $(INCLUDES) $< -o $@
	$(call show,,reset)

$(BLD_DIR)/lex_%.o: $(OUT_DIR)/lex_%.c
	$(call show,blue)
	$(CC) -c $(INCLDS) $(LIBS) $(CFLAGS) $(INCLUDES) $< -o $@
	$(call show,,reset)

$(BLD_DIR)/yacc_%.o: $(OUT_DIR)/yacc_%.c
	$(call show,blue)
	$(CC) -c $(INCLDS) $(LIBS) $(CFLAGS) $(INCLUDES) $< -o $@
	$(call show,,reset)

$(BLD_DIR)/%.o: %.c
	$(call show,blue)
	$(CC) -c $(INCLDS) $(LIBS) $(CFLAGS) $(INCLUDES) $< -o $@
	$(call show,,reset)

$(BIN_DIR)/$(PROGRAM): $(CLEX) $(CYACC) $(OBJS)
	$(call show,green,bold)
	$(CC) $(INCLDS) $(LIBS) $(CFLAGS) $(INCLUDES) $(wildcard $(BLD_DIR)/*.o) -o $@
	$(call show,,reset)

build: setup $(BIN_DIR)/$(PROGRAM)

run: build
	@./$(BIN_DIR)/$(PROGRAM) argumento1 "string 1" "string 2"

fmt:
	@echo "C and Headers files:"
	$(call show,yellow)
	@-clang-format -verbose -i $(SRC_DIR)/*.c $(INC_DIR)/*.h
	$(call show,,reset)
	@echo ""
	@echo "Shell files:"
	$(call show,yellow)
	@shfmt -w -i 2 -l -ci .
	$(call show,,reset)

lint:
	@splint -retvalint -hints -I $(INC_DIR) \
		$(SRC_DIR)/*

debug: CFLAGS = -Wall -Wextra -ansi -pedantic -O0 -g
debug: build
	gdb ./$(BIN_DIR)/$(PROGRAM)

doc:
	@doxygen $(DOC_DIR)/Doxyfile

test: build
	@./$(TST_DIR)/test_input.sh

setup:
	@mkdir -p $(BIN_DIR)
	@mkdir -p $(BLD_DIR)
	@mkdir -p $(LOG_DIR)
	@mkdir -p $(OUT_DIR)

clean:
	@echo "Cleaning..."
	@echo ""
	@cat .art/maid.ascii
	@-rm -rf $(TRASH)
	@echo "...✓ done!"
