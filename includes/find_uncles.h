#ifndef __FIND_UNCLES_H__
#define __FIND_UNCLES_H__

#include "global.h"
#include "structs.h"

void findUncles(GHashTable *, GHashTable *);

#endif
