#ifndef __GLOBAL_H__
#define __GLOBAL_H__

#define URI 0
#define INT 1
#define FLOAT 2
#define STR 3

enum Gender { Male, Female, Unknown };

enum Type {
    temAvoM,
    temAvoF,
    temPrimo,
    temPrima,
    temTio,
    temTia,
    temIrmao,
    temIrma,
    temMae,
    temPai
};

union objectValue {
    int intVal;
    float fltVal;
    char *strVal;
};

#endif
