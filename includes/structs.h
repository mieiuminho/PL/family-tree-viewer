#ifndef __STRUCTS_H__
#define __STRUCTS_H__

#define _GNU_SOURCE

#include "global.h"
#include <glib.h>
#include <gmodule.h>

typedef struct pais {
    char *pai;
    char *mae;
} * pais;

typedef struct subjectparents {
    char *subject;
    char *mae;
    char *pai;
} * subjectparents;

typedef struct personalInfo {
    char *personURI;
    enum Gender gender;
    GSList *relationships;
} * personalInfo;

typedef struct relationship {
    char *targetURI;
    enum Type relation;
} * relationship;

typedef struct elem {
    union objectValue valor;
    int type;
} * elem;

#endif
