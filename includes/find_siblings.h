#ifndef __FIND_SIBLINGS_H__
#define __FIND_SIBLINGS_H__

#include "global.h"
#include "structs.h"

#define _GNU_SOURCE

void findSiblings(GHashTable *, GHashTable *);

#endif
