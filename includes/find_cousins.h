#ifndef __FIND_COUSINS_H__
#define __FIND_COUSINS_H__

#define _GNU_SOURCE

#include <glib.h>
#include <gmodule.h>

void findCousins(GHashTable *, GHashTable *);

#endif
