#ifndef __FIND_GRANDPARENTS_H__
#define __FIND_GRANDPARENTS_H__

#include "global.h"
#include "structs.h"

void findGrandparents(GHashTable *, GHashTable *);

#endif
