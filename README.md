[hugo]: https://github.com/HugoCarvalho99
[hugo-pic]: https://github.com/HugoCarvalho99.png?size=120
[nelson]: https://github.com/nelsonmestevao
[nelson-pic]: https://github.com/nelsonmestevao.png?size=120
[pedro]: https://github.com/pedroribeiro22
[pedro-pic]: https://github.com/pedroribeiro22.png?size=120

# Visualizador de Árvores Genealógicas

TODO: write small description

## :rocket: Getting Started

These instructions will get you a copy of the project up and running on your
local machine for development and testing purposes.

### :inbox_tray: Prerequisites

The following software is required to be installed on your system:

- [make](https://www.gnu.org/software/make/)
- [gcc](https://gcc.gnu.org/releases.html)
- [flex](https://github.com/westes/flex)
- [yacc](https://www.gnu.org/software/bison/)

### :package: Installation

TODO: explain installation process

### :video_game: Usage

TODO: show some examples on usage

### :hammer: Development

Compile the program. You only need to run `init` the very first time or
every time after a `clean`. The `build` target it's optional.

```
make init
make build
```

Run the tests.

```
make test
```

Format your code.

```
make fmt
```

Generate the documentation.

```
make doc
```

## :busts_in_silhouette: Team

| [![Hugo][hugo-pic]][hugo] | [![Nelson][nelson-pic]][nelson] | [![Pedro][pedro-pic]][pedro] |
| :-----------------------: | :-----------------------------: | :--------------------------: |
|   [Hugo Carvalho][hugo]   |    [Nelson Estevão][nelson]     |    [Pedro Ribeiro][pedro]    |
